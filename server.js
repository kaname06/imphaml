//requiring and instancing const
const express = require('express');
const app = express();
const excel = require('node-excel-export');
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const cParser = require('cookie-parser')
const updateJsonFile = require('update-json-file')
const fs = require('fs')

//database connect
mongoose.connect('mongodb://127.0.0.1:27017/binary', { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true})
.then(db => console.log("Database Connected Successfully"))
.catch(err => console.error("Houston, we have a problem here: \n"+err))


//settings
app.set('PORT', process.env.PORT || 2802);

//Middlewares
app.use(express.json())
app.use(cParser())
app.use(session({
    key: 'user_session_id',
    secret: '10n1c0s3cr3tc0d3',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: (60*60*1000)
    }
}))

app.get('/', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        if(req.session.user.userType != 'worker')
            res.sendFile(path.join(__dirname, '/public/index.html'));
        else
            res.redirect('/orders');    
    } 
    else {
        res.redirect('/auth/login');
        // res.sendFile(path.join(__dirname, '/public/login.html'));
    }
})
app.get('/orders', (req,res) => {
    if (req.session.user && req.cookies.user_session_id && req.session.user.userType == 'worker') {
       res.sendFile(path.join(__dirname, '/public/pays.html'));
    } else {
        res.redirect('/');
    }
    // res.sendFile(path.join(__dirname, '/public/cellar.html'));
})

app.get('/logout', (req, res) => {
    let json = JSON.parse(fs.readFileSync('u23r20nl1n3.txt'));
    let user = '';
    if(req.session.user){
        user = req.session.user.dni.dni
    }
    for (const key in json) {
        if(json[key].dni == user) {
            // cookies.remove('room')
            updateJsonFile('u23r20nl1n3.txt', (dat) => {
                dat.splice(key, 1) 
                return dat;
            })
            break;
        }
    }
    req.session.user = null
    res.redirect('/auth/login');
})


//initializations
updateJsonFile('u23r20nl1n3.txt', (dat) => {
    dat = []
    return dat;
})
updateJsonFile('P4y5L0g.txt', (dat) => {
    dat = []
    return dat;
})

//CRON JOBS
require('./crons')

//Routes
app.use('/auth', require('./src/Routes/auth').router)
app.use('/pays', require('./src/Routes/payorder').router)

//Public path
app.use(express.static(path.join(__dirname, '/public')));

//Server listen callback
app.listen(app.get('PORT'), () => {
    require('./src/Routes/auth').seedData()
    console.log(`Server running on port: ${app.get('PORT')}`);
})

app.get('/export/excel/:s/:e', async (req, res) =>
{
    let a = await require('./src/Routes/payorder').exportt(req.params.s, req.params.e)
    console.log(a.name)
    const report = excel.buildExport([
        {
            name: a.name,
            heading: a.h,
            merges: a.m,
            specification: a.s,
            data: a.d
        }
    ]);
    res.attachment(a.name+'.xlsx');            
    return res.send(report);
})