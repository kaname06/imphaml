const jsmd5 = require('js-md5')

const md5Plugin = {}

md5Plugin.install = function(Vue)
{
    Vue.prototype.$jsmd5 = jsmd5
}

Vue.use(md5Plugin)
