window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.$ = window.jQuery = require('jquery');
require('jquery-ui-dist/jquery-ui')
$('[data-toggle="tooltip"]').tooltip()
// window.dt = require( 'datatables.net' )();
window.Vue = require('vue');
let cryptorjs = require('./Utilities/cryptor');
let jsmd5 = require('./Utilities/md5')
// let chidoriAlert = require('./Utilities/Plugins/chidoriAlert');
let swal = require('./Utilities/sweetAlert')

import VueRouter from 'vue-router';
// import picker from 'vuejs-datepicker';

// import vSelect from 'vue-select';
// import VModal from 'vue-js-modal'
 
// Vue.use(VModal, { dialog: true })

// Vue.use(picker);
Vue.use(VueRouter);

//pagination 
// import VuePaginate from 'vue-paginate';
// Vue.use(VuePaginate); 
//tree
// import {tree} from 'vued3tree';
// Vue.use(tree)
//data table UI
//Source
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
//language
import lang from 'element-ui/lib/locale/lang/es'
import locale from 'element-ui/lib/locale'
const DataTables = require('vue-data-tables') 

locale.use(lang)
Vue.use(DataTables)

import main from './Components/MainComponent.vue'
import galaxyC from './Components/GalaxyComponent.vue';
import register from './Components/RegisterComponent.vue';
import login from './Components/LoginComponent.vue';
import profile from './Components/ProfileComponent.vue';
import panel from './Components/ListUserComponent.vue';
import ordersp from './Components/OrderPaysComponent.vue';
import histo from './Components/HistoryPaysComponent.vue';
import navbar from './Components/navbarComponent.vue';
import mainpay from './Components/MainPayComponent.vue';
import points from './Components/PointsComponent.vue';
// import count from './Components/CountDownComponent.vue';
import mypays from './Components/ConsignationsComponent.vue';
import probuy from './Components/PurchasesComponent.vue';
// import sidebar from './Components/sidebarComponent.vue';
// import treev from './Components/TreeComponent.vue';



let paths = {
    main,
    galaxyC,
    register,
    profile,
    login,
    panel,
    ordersp,
    histo,
    navbar,
    mainpay,
    points,
    mypays,
    probuy,
    // count,
    // sidebar
    // treev
   
   
   
}

let galaxyRs = [
    // { path: '/lobby', component: paths.main},
    { path: '/constellation', component: paths.galaxyC},
    { path: '/r/:id?', component: paths.register},
    { path: '/profile', component: paths.profile},  
    { path: '/points', component: paths.points},
    { path: '/mypays', component: paths.mypays}, 
    { path: '/purchases', component: paths.probuy},   
    // { path: '/', component: paths.count},   
    // { path: '/treeview', component: paths.treev}
]
let orders = [
  { path: '/orderspays', component: paths.ordersp},
  { path: '/history', component: paths.histo},
  { path: '/', component: paths.mainpay}
]
  
// let auth = [{ path: '/login', component: paths.login}]
const galaxy = new VueRouter({
    uid: 'main',
    // mode: 'history',   
    routes: galaxyRs // short for `routes: routes`
})
const op= new VueRouter({
  uid: 'pays',
  // mode: 'history',   
  routes: orders// short for `routes: routes`
})

Vue.component('main-component', require('./Components/MainComponent.vue').default);
Vue.component('register-component', require('./Components/RegisterComponent.vue').default);
Vue.component('galaxy-component', require('./Components/GalaxyComponent.vue').default);
Vue.component('login-component', require('./Components/LoginComponent.vue').default);
Vue.component('profile-component',require('./Components/ProfileComponent.vue').default);
// Vue.component('listuser-component',require('./Components/ListUserComponent.vue').default);
Vue.component('orderpays-component',require('./Components/OrderPaysComponent.vue').default);
Vue.component('history-component',require('./Components/HistoryPaysComponent.vue').default);
Vue.component('navbar-component',require('./Components/navbarComponent.vue').default)
Vue.component('mainpay-component', require('./Components/MainPayComponent.vue').default);
// Vue.component('countdown-component', require('./Components/CountDownComponent.vue').default);
// Vue.component('print-component', require('./Components/PrintOrdersComponent.vue').default);
// Vue.component('tree-component',require('./Components/TreeComponent.vue').default);
const mainapp = new Vue({
    el: '#main'
});
const universe = new Vue({
    el: '#constellation',
    router: galaxy
  });
  const acc = new Vue({
    el: '#panel'
  });
  const opmf = new Vue({
    el: '#pays',
    router: op
  }); 


