const Order = require ('../Models/orderPaymentModel')
const pays = require('../Models/orderPaymentModel');

class table {
    store(request)
    {
        let Data = new Order(request)
        //console.log(request)
        var error = Data.validateSync();
        if(error) {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;            
        } else{
            let status = Data.save();            
            return status;            
        }
    }
    async addDeductions(info){
        let id = info.idop
        let deducpay = info.value
        let netopay = info.neto
        let deducDetail = info.data
        let data_ = await pays.findOne({_id:id})
        if(data_){
            data_.deductions = deducpay
            data_.deductionsDetails.push(deducDetail)
            data_.neto = netopay
             let po = await data_.save();
             return po 
            }   
    }
    async delDeductions(info){
        let idpos = info.idPos
        let id = info.idop
        let total = info.neto
        let taxTl = info.tax
        let pos = ''
        let data_ = await pays.findOne({_id:id})
        if(data_){
            for (let index = 0; index < data_.deductionsDetails.length; index++) {
                let id = data_.deductionsDetails[index]._id
                if(id == idpos)
                {
                    pos = index
                }                
            }
            data_.deductionsDetails.splice(pos,1) 
            data_.deductions = taxTl
            data_.neto = total
             let po = await data_.save();
             return po 
            }   
    }
     
   async upstatusPays(info){
    let id = info.idop
    let netopay = info.neto
    let deducpay = info.deductions
    let data_ = await pays.findOne({_id:id})
    if(data_){
        data_.status = "payout"
        // data_.neto = netopay
        // data_.deductions = deducpay
         let po = await data_.save();
         return po 
        }   
    }
}
let order = new table();
module.exports = {order}; 