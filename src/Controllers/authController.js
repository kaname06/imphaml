const auth = require ('../Models/authModel')
const bin = require('../Models/binaryModel')
const product = require('../Models/productModel');
const category = require('../Models/productCategoryModel')

class table 
{
    constructor() {
        this.persons = [];
        this.finded = [];
        this.founded = [];
        this.count = 1;
    }

    async store(request)
    {
        request.accessData.password = auth.encryptPass(request.accessData.password)
        let data = new auth(request)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return 'Fatal Error, unable to store person, try later'
            }else {
                if(data.pattern != null) {
                    let { asocSon } = await this.AddDirectSon(data.pattern, request.side, data._id)
                    if(asocSon) {
                        return status;
                    }
                    else {
                        await auth.findByIdAndRemove(data._id);
                        return 'Fatal Error, unable to store person, postion taked'
                    }
                }
                else {
                    return status
                }
                
            }
            // return status;
        }
    }

    async UpBA(data) {
        let id = data.id;
        let bData = data.data;

        let dat = await auth.findById(id);
        dat.bankAccount = bData;
        let result = await dat.save();
        return result;
    }
    async UpInfoBasic(data) {
        
        let id = data.id;
        let bData = data.data;

        let dat = await auth.findById(id);
        dat = bData;
        let result = await dat.save();
        return result;
    }
    async AddSon(father, sons) {
        let data = await auth.findById(father);
        data.sonsNumber = parseInt(data.sonsNumber) + (parseInt(sons))
        let result = await data.save()
        let res = false
        if(!('_id' in result)) {
            res = false
        }
        else
            res = true
            if(data.pattern != null)
                this.AddSon(data.pattern, sons)
        return res
    }

    async AddDirectSon(father, side, sonId) {
        let dad = await auth.findById(father);
        if(dad.sons[side] == null) {
            dad.sons[side] = sonId
            let result = await dad.save()

            let asocSon = false
            if(!('_id' in result)){
                
            }else {
                let cb = this.AddSon(father, 1);
                if(cb) {
                    asocSon = true
                }
            }
            return {result, asocSon}
        }
        else
            return {result:'failed',asocSon: false}
    }

    async killPerson() {
        let data = await auth.find({'accessData.live': 'Dying'});
        let deads = []
        for (let index = 0; index < data.length; index++) {
            let pd = new Date(data[index].created_at)
            pd.setFullYear(pd.getFullYear() + 1)
            // pd.setDate(pd.getDate() + 2)
            let cd = new Date()
            let dif = cd - pd
            if((dif/(1000*60*60*24*365)) > 0){
                let person = await auth.findById(data[index]._id);
                person.accessData.live = 'Dead'
                let stat = person.save()
                if(!('_id' in stat)){}
                else {
                    let name = stat.name.first + " " + stat.name.last
                    deads.push(name)
                }
            }
        }
        return deads
    }

    async setStatePerson(id, newpaydate = null, state, paydate = null){
        let data = await auth.findOne({_id: id})
        data.accessData.status = state
        if(state)
            data.accessData.live = 'Live'
        else{
            let datai = await bin.findOne({person: id})
            datai.ownP = 0
            await datai.save()
        }
        if(newpaydate != null && state) {
            data.accessData.programmedPay = newpaydate
            data.accessData.lastPay = paydate
        }
        return await data.save()
    }

    async turnDownPerson(id) {
        let data = await auth.findOne({_id: id})
        // let b = await auth.deleteOne({_id: id})
        let b = {
            ok: 0,
            deletedCount: 0
        }
        data.accessData.live = 'Dying'
        let B = await data.save();
        if(!('_id' in B)){}
        else {
            b.ok = 1
            b.deletedCount = 1
        }
        if(b.ok == 1 && b.deletedCount == 1){
            let p = await auth.findById(data.pattern)
            if(p){
                // console.log('entra a lo del pattern')
                if(p.sons.left == id.toString()){
                    // console.log('entra left')
                    p.sons.left = null
                }
                if(p.sons.right == id.toString()){
                    // console.log('entra right')
                    p.sons.right = null
                }
                await p.save()

                let Orphans = await auth.find({realPattern: data._id});
                for (let index = 0; index < Orphans.length; index++) {
                    let orphan = await auth.findOne({_id: Orphans[index]._id});
                    orphan.realPattern = data.realPattern
                    await orphan.save()
                }
            }
            b.person = data.name.first
            this.AddSon(data.pattern, -1)
            return {status: true, data: b}
        }
        return {status: false}
    }

    clearCount() {
        this.count = 1;
    }

    getCount() {
        return this.count;
    }

    async countSide(id) {
        let data = await auth.findById(id);
        if(data.accessData.live != 'Live')
            this.count--;
        if(this.finded.length == 0)
            this.founded.push(id)
        this.finded.push(id)
        if(data.sons.left) {
            this.count++;
            this.founded.push(data.sons.left)
        }
        if(data.sons.right) {
            this.count++;
            this.founded.push(data.sons.right)
        }
        let i = 0
        for (let index = 0; index < this.founded.length; index++) {
            if(this.finded.includes(this.founded[index])) {
                i++
            }else{
                break;
            }
        }
        if(this.finded.length == this.founded.length)
            return this.count
        else
            await this.countSide(this.founded[i])
        
        return 'finalized'
    }

    clearVector(){
        this.persons = []
        this.finded = []
        this.founded = []
    }

    getVector() {
        return this.persons
    }

    parseTreeData() {
        let data = this.persons;
        let dataCrop = data;
        let result = null;
        let counter = 0 
        let reference = data.length - 1 
        let td = reference
        let aux1 = data[data.length - 1];
        let aux2 = null;
        for (let index = 0; index < dataCrop.length; index++) {
            // dataCrop[index].children = {left: null, right: null}
            dataCrop[index].children = []
        }
        while(dataCrop.length > 1) {
            for (let index = 0; index < dataCrop.length; index++) {
                if(aux1.pattern != null){
                    if(dataCrop[index]._id == aux1.pattern.toString()) {
                        // console.log(aux1.position == 'left')
                        aux2 = dataCrop[index];
                        // if(aux1.position == 'right')
                        //     aux2.children.right = aux1
                        // if(aux1.position == 'left')
                        //     aux2.children.left = aux1
                        aux2.children.push(aux1)
                        for (let index = 0; index < dataCrop.length; index++) {
                            if(dataCrop[index]._id == aux1._id)
                                dataCrop.splice(index, 1)
                        }
                        break;
                    }
                }
                else
                    break;
            }

            td--;
            counter++;
            if(aux2.pattern != null)
                aux1 = aux2
            else
                aux1 = dataCrop[dataCrop.length - 1]
            // console.log(dataCrop.length)
        }
        result = aux2;
        return result;
    }

    structureData(data, posi = null) {
        let lvl = 0
        let position = posi
        if(data.pattern) {
            for (let index = 0; index < this.persons.length; index++) {
                if(this.persons[index]._id == data.pattern.toString()) {
                    lvl = this.persons[index].level + 1
                    if(this.persons[index].sons.left == data._id.toString()){
                        position = "left"
                    }
                    else {
                        if(this.persons[index].sons.right == data._id.toString()){
                            position = "right"
                        }
                    }
                }
            }
        }
        
        let ret = {
            _id: data._id,
            name: data.name,
            dni: data.dni,
            level:lvl,
            sons: data.sons,
            nsons: data.sonsNumber,
            bankAcc: data.bankAccount,
            pattern: data.pattern,
            position: position,
            realPattern: data.realPattern,
            code: data.accessData.userCode,
            avatar: data.accessData.avatar,
            status: data.accessData.status,
            userType: data.accessData.userType,
            address: data.address,
            mail: data.mail
        }
        return ret
    }
    async findPerson(id) {
        let data = await auth.findById(id);
        return data
    }

    async findAllTree(id, pos = null) {
        let data = await this.findPerson(id)
        
        if(data) {
            if(this.finded.length == 0)
                this.founded.push(id)
            this.finded.push(id)
            this.persons.push(this.structureData(data, pos))
            if(data.sons.left != null)  {
                this.founded.push(data.sons.left)
                // await this.findPerson(data.sons.left); 
            }
            if(data.sons.right != null) {
                this.founded.push(data.sons.right)
                // await this.findPerson(data.sons.right)
            }
        }
        let i = 0
        for (let index = 0; index < this.founded.length; index++) {
            if(this.finded.includes(this.founded[index])) {
                i++
            }else{
                break;
            }
        }
        if(this.finded.length == this.founded.length)
            return this.persons
        else
            await this.findAllTree(this.founded[i])
        
        return 'finalized'
    }
    async getAllPro(onlyActive) {
        let data;
        if(onlyActive)
            data = await product.find({status: true, stock: {$gt: 0}}).sort({created_at: 1})
        else
            data = await product.find().sort({created_at: 1})
        let newData = []
        for (const key in data) {
            let catName = await category.findOne({_id: data[key].category}, {_id: 0, name: 1})
            let bone = {
                _id: data[key]._id,
                name: data[key].name,
                code: data[key].code,
                sanitaryReg: data[key].sanitaryReg,
                description: data[key].description,
                slogan: data[key].slogan,
                prices: data[key].prices,
                stock: data[key].stock,
                status: data[key].status,
                images: data[key].images,
                category: catName.name
            }
            newData.push(bone)
        }
        return newData
    }
}
let Auth = new table();
module.exports= {Auth};