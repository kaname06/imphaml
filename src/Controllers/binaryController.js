const bin = require('../Models/binaryModel');
const auth = require('../Models/authModel');
const purchs = require('../Models/purchaseModel');

class table {
    store(request)
    {
        let Data = new bin(request)
        //console.log(request)
        var error = Data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return status;            
        } else{
            let status = Data.save();            
            return status;            
        }
    }

    async upAddPoints(id, points, son) {
        let person = await auth.findById(id);
        //console.log(person.name.first+' up do it')
        let data = await bin.findOne({person: id})
        if(!data) {
            let bone = {
                person: id,
                ownP: 0,
                downP: 0
            }
            let res = await this.store(bone)
            if(!('_id' in res)){return false;}
            else{
                data = res
            }
        }
        if(person.sons.left == son.toString()){
            //console.log('izq')
            data.tptLeft = data.tptLeft + (points)
        }
        if(person.sons.right == son.toString()){
            //console.log('der')
            data.tptRight = data.tptRight + (points)
        }
        // console.log(data)
        let result = await data.save()
        let res = false
        if(!('_id' in result)) {
            res = false
        }
        else
            res = true
        if(person.pattern != null)
            this.upAddPoints(person.pattern, points, person._id)
        return res
    }
    async downAddPoints(id, points) {
        let person = await auth.findById(id);
        //console.log(person.name.first)
        let data = await bin.findOne({person: id})
        let res = false
        if(!data) {
            let bone = {
                person: id,
                ownP: 0,
                downP: 0
            }
            let res = await this.store(bone)
            if(!('_id' in res)){return false;}
            else{
                data = res
            }
        }
        points = points + data.downP
        if(person.sons.right != null){
            if(person.sons.left != null){
                if(data.tptRight > data.tptLeft){
                    data.tptLeft = data.tptLeft + points
                    data.downP = 0
                    let result = await data.save()
                    if(!('_id' in result)) {
                        res = false
                    }
                    else{
                        res = true
                        await this.downAddPoints(person.sons.left, points)
                    }
                }else{
                    if(data.tptRight < data.tptLeft) {
                        data.tptRight = data.tptRight + points
                        data.downP = 0
                        let result = await data.save()
                        if(!('_id' in result)) {
                            res = false
                        }
                        else{
                            res = true
                            await this.downAddPoints(person.sons.right, points)
                        }
                    }else {
                        if(data.tptRight == data.tptLeft) {
                            data.tptRight = data.tptRight + points
                            data.downP = 0
                            let result = await data.save()
                            if(!('_id' in result)) {
                                res = false
                            }
                            else{
                                res = true
                                await this.downAddPoints(person.sons.right, points)
                            }
                        }
                    }

                }
            }else{
                data.tptRight = data.tptRight + points
                data.downP = 0
                let result = await data.save()
                if(!('_id' in result)) {
                    res = false
                }
                else{
                    res = true
                    await this.downAddPoints(person.sons.right, points)
                }
            }
        }
        else {
            if(person.sons.left != null){
                data.tptLeft = data.tptLeft + points
                data.downP = 0
                let result = await data.save()
                if(!('_id' in result)) {
                    res = false
                }
                else{
                    res = true
                    await this.downAddPoints(person.sons.left, points)
                }
            }
            else {
                data.downP = points
                let result = await data.save()
                if(!('_id' in result)) {
                    res = false
                }
                else{
                    res = true
                }
            }
        }
        //console.log('down finished')
        return res
    }

    async getPoints() {
        let data = await purchs.find({pointsUsed: false, purchaseStatus: 'Approved'});
        if(data.length == 0)
            return null
        for (let index = 0; index < data.length; index++) {
            let per = await bin.findOne({person: data[index].buyer});
            let points = data[index].totalPoints
            if(per){
                if(per.ownP == 0){
                    if(points <= 50) {
                        per.ownP = points
                        let p = await purchs.findOne({_id: data[index]._id})
                        p.pointsUsed = true
                        await p.save()
                        await per.save()
                        return false;
                    }else{
                        per.ownP = 50
                        let ppt = points - 50;
                        await this.downAddPoints(data[index].buyer, ppt)
                    }
                }else{
                    await this.downAddPoints(data[index].buyer, points)
                }
                let re = await per.save()
                if(!('_id' in re)){}
                else{
                    let p = await purchs.findOne({_id: data[index]._id})
                    p.pointsUsed = true
                    await p.save()
                }
            }
            else {
                let op = 0
                let tp = 0
                let pp = 0
                if(points <= 50)
                    op = points
                else {
                    op = 50
                    let persipersi = await auth.findById(data[index].buyer)
                    if(persipersi.sons.right != null || persipersi.sons.left != null)
                        pp = points - 50
                    else
                        tp = points - 50
                }
                let bone = {
                    person: data[index].buyer,
                    ownP: op,
                    downP: tp
                }
                let res = await this.store(bone)
                if(!('_id' in res)){}
                else{
                    if(points > 50)
                        await this.downAddPoints(data[index].buyer, pp)
                    let p = await purchs.findOne({_id: data[index]._id})
                    p.pointsUsed = true
                    await p.save()
                }
            }
            
            let name = await auth.findById(data[index].buyer)
            if(name && name.pattern){
                await this.upAddPoints(name.pattern, points, name._id)
            }
            return 'Points translated to '+name.name.first
        }
    }
}

let Bin = new table()
module.exports = {Bin}