const { Router } = require('express');
const updateJsonFile = require('update-json-file')
var cryptorjs = require('cryptorjs')
var myCryptor = new cryptorjs('KaioKenx100alafergatodo');
const path = require('path')
require('express-router-group')
const excel = require('node-excel-export');
const axios = require('axios');

router = Router();
//Auth files
const { Auth } = require('../Controllers/authController');
const auth = require('../Models/authModel');

//Purchases files
const purchasem = require('../Models/purchaseModel')

// Order Payment files

const {order} = require('../Controllers/orderPaymentController')

//Pay Files
const pays = require('../Models/orderPaymentModel');


// router.get('/orders', (req, res) => {
//     //validar que solo sea el usauario que es - validar sesion donde solo maria fernanda tenga acceso
//     if (req.session.user && req.cookies.user_session_id && req.session.user.userType == 'worker') {
//         // res.redirect('/') //indicar ruta de lobby de order
//         res.sendFile(path.join(__dirname,'../../public/pays.html'));
//     }
//     else
//     {
//         res.sendFile(path.join(__dirname,'../../public/login.html')); 
//     }
// })
//Ordenes de pago
router.post('/orderpay',async (req,res) => { 
    let order = await pays.find({status:'topay'}).populate('person')
    let payorder = myCryptor.encode(order)
    let status = 'success'
    if(order)
    {
        res.json({status,data:payorder})
    }
    else{
        res.json({status:'failed'})
    }
})
router.post('/insertorder',async (req,res) => {
    let data_ = req.body.data
    let reg = await order.store(data_)
    let status = 'failed'
    if(('_id' in reg))
    {
        status = 'success'
    }
    res.json({
        'status':status,
        'data':reg
    })
})

router.post('/payout', async (req,res)=>{
    let data_ = myCryptor.decode(req.body.data)
    // console.log('data_:',data_)
    let result = await order.upstatusPays(data_)
    console.log(result)
    let status = 'failed'
    if('_id' in result){
        status = "success"
    }
    res.json({
        status,
        data: result
    })
})
router.post('/update-expired-status', async (req,res)=>{
    let id = req.body.idop
    let st = req.body.status
    let e = await pays.findByIdAndUpdate(id, st)
    let status = 'failed'
    if('_id' in e){
        status = "success"
    }
    res.json({
        status,
        e
    })
})

router.post('/show', async (req, res)=>{
    let data = await pays.find({$or: [{status:'payout'}, {status: 'expired'}]}).sort({created_at:-1})
    let datae = myCryptor.encode(data)
    let status = 'success'
    if(!data)
    {
        status = 'failed'
    }
    res.json({
        status,
        data: datae
    })
})
router.post('/insertdeductions', async (req,res) => {
    let data = myCryptor.decode(req.body.data)
    // console.log(data)
   let deduc = await order.addDeductions(data)
    if('_id' in deduc)
    {   
    
        res.json({status:'success',data:myCryptor.encode(deduc.deductionsDetails)})
    }
    else {
        res.json({status:'failed'})
    }

})
router.post('/removeDeduction', async (req,res) => {
    let data = myCryptor.decode(req.body.data)
    let data_ = await order.delDeductions(data)
    if('_id' in data_)
    {
       let newInfo = {
           deductions: data_.deductions,
           neto: data_.neto,
           deductionsDetails: data_.deductionsDetails
       }
        res.json({status:'success',data:myCryptor.encode(newInfo)})
    }
    else
    {
        res.json({status:'failed'})
    }


})

router.get('/print', (req, res) => {    
    res.sendFile(path.join(__dirname,'../../public/print.html'));
})

let exportt = async (s, e) => 
{
    var startDate = new Date(s);
    var endDate = new Date(e);
    let dat = []
    let bon = 
    {
        amount: '',
        person: '',
        deductions: 0,
        neto: '',
        status: '',
        created: '',
        dni: '',
        nc: '',
        bk: ''
    }
    let data = await pays.find({status: 'payout', 'created_at' : {$gt: startDate, $lt: endDate}})
    let dataP = await auth.find();    
    if(dataP == null || dataP.length <= 0){
        return ({status:'failed', data: 'nada'})
    }else 
    {
        for (const key in data) 
        {
            bon.amount = data[key].amount.toLocaleString('de-DE')
            bon.created = data[key].created_at
            bon.neto = data[key].neto.toLocaleString('de-DE')
            bon.status = 'Autorizado'
            let d = data[key].deductionsDetails                          
            bon.deductions = data[key].deductions.toLocaleString('de-DE')                            
            for (const keyy in dataP) 
            {                
                if(data[key].person == (dataP[keyy]._id).toString())
                {
                    bon.person = dataP[keyy].name.first + ' ' + dataP[keyy].name.last
                    bon.dni = dataP[keyy].dni.dni
                    bon.nc = dataP[keyy].bankAccount.account
                    bon.bk = dataP[keyy].bankAccount.bank
                }
            }
            dat.push(bon)
            bon = 
            {
                amount: '',
                dni: '',
                person: '',
                deductions: 0,
                neto: '',
                status: '',
                created: ''
            }
        }        
        const styles = 
        {
            headerFills: 
            {
                border: 
                {
                    left: { style: 'medium', color: 'ffffff' },
                    right: { style: 'medium', color: 'ffffff' }
                },
                fill: 
                {
                    fgColor: {
                      rgb: '000000'
                    }
                },
                font: {
                    color: {
                        rgb: 'ffffff'
                    },
                    sz: 14,
                    bold: true,
                    underline: false
                },
                alignment: {
                    horizontal: 'center',
                    vertical: 'center'
                }
            },
            headerNormal: 
            {
                font: {
                    color: {
                        rgb: '000000'
                    },
                    sz: 14,
                    bold: true,
                    underline: true
                }
            },
            headerContent: 
            {
                font: {
                    color: {
                        rgb: '000000'
                    },
                    sz: 17,
                    bold:true
                },
                alignment: {
                    horizontal: 'center'
                }
            },
            cell: 
            {
                alignment: {
                    horizontal: 'center'
                }
            }
        }
        
        //Array of objects representing heading rows (very top)
        rr = startDate.getDate() +'/'+(startDate.getMonth()+1)+'/'+startDate.getFullYear()+' - '+endDate.getDate()+'/'+(endDate.getMonth()+1)+'/'+endDate.getFullYear()
        const heading = [
            [{value: 'IMPHA', style: styles.headerContent}],
            [{value: 'REPORTE DE ORDENES DE PAGO (PAGADAS)', style: styles.headerContent}],
            [{value: rr, style: styles.headerContent}],
            [{value: '-', style: styles.headerContent}],
        ];
        
        const specification = {
            dni: {
                displayName: 'Identificación',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 200,
                heigth: 50
            },
            person: {
                displayName: 'Nombre',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 250
            },
            neto: {
                displayName: 'Neto',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 100
            },
            amount: {
                displayName: 'Cantidad',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 100
            },
            status: {
                displayName: 'Estatus',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 100
            },
            deductions: {
                displayName: 'Total Deducciones',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 200
            },
            nc: {
                displayName: 'N° Cuenta',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 200
            },
            bk: {
                displayName: 'Banco',
                headerStyle: styles.headerFills,
                cellStyle: styles.cell,
                width: 200
            }
        }
    let maxl = data.length
    const merges = [
        {start: { row: 1, column: 1}, end: { row: 1, column: (8) }},
        {start: { row: 2, column: 1}, end: { row: 2, column: (8) }},
        {start: { row: 3, column: 1}, end: { row: 3, column: (8) }},
        {start: { row: 4, column: 1}, end: { row: 4, column: (8) }}
    ]
    const dataset = dat
    let nam = 'Reporte ordenes de pago'
    
    return ({name: nam, d: dataset, h: heading, m: merges, s: specification})    
    }
}

module.exports = {router, exportt}