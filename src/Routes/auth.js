const { Router } = require('express');
const updateJsonFile = require('update-json-file')
const fs = require('fs')
var cryptorjs = require('cryptorjs')
var myCryptor = new cryptorjs('KaioKenx100alafergatodo');
const path = require('path')
require('express-router-group')
var bcrypt = require('bcryptjs');

router = Router();
//Auth files
const { Auth } = require('../Controllers/authController');
const auth = require('../Models/authModel');

//Purchases files
const purchasem = require('../Models/purchaseModel')

// Order Payment files

const {order} = require('../Controllers/orderPaymentController');
const pays = require('../Models/orderPaymentModel');

// points Binary
const {Bin} = require('../Controllers/binaryController')
const bin = require('../Models/binaryModel');

//Worker files

const worker = require('../Models/workerModel')

//Count sons function
let counter = async (id) => {
    if(id != null) {
        let result = await Auth.countSide(id)
        if(result != null && result == 'finalized') {
            let num = Auth.getCount();
            Auth.clearCount()
            Auth.clearVector()
            return num
        }
        else {
            Auth.clearCount()
            Auth.clearVector()
            return null
        }
    }
    else
        return null
}
//finish prev comm

//count direct sons
let directCounter = async (id) => {
    let data = await auth.countDocuments({realPattern: id});
    return data
}
//finish prev comm

router.get('/login', (req, res) => {
    if (req.session.user && req.cookies.user_session_id) {
        res.redirect('/')
    }
    else{
        res.sendFile(path.join(__dirname,'../../public/login.html'));
    }
    
})
// router.get('/upaff', (req, res) => {
//     //validar que solo sea el usauario que es
//     res.sendFile(path.join(__dirname,'../../public/manager.html'));
// })

router.post('/store', async (req, res) => {
    let data_ = myCryptor.decode(req.body.data);    
    // let data_ = req.body
    let p = data_.dni.dni.substring(data_.dni.dni.length -5)
    data_.accessData.userCode = data_.name.first.substring(0,1).toLowerCase() + data_.name.last.substring(0,1).toLowerCase() + data_.dni.dni.toLowerCase()
    data_.accessData.password = p
    let data = await Auth.store(data_);
    if(typeof data === 'string' || !('_id' in data)) {
        res.json({status: 'failed', data})
    }
    else {
        // Auth.AddDirectSon(data.pattern, data_.side, data._id)
        res.json({status: 'success', data, access: {user: data.accessData.userCode, password: p}})
    }
})

router.group('/update', router => {
    router.post('/bank-account', async (req, res) => {
        let data_ = myCryptor.decode(req.body.data)
        //let data_ = req.body.data
        // console.log(data_)
        let data = await Auth.UpBA(data_);
        if(!('_id' in data)) {
            res.json({status: 'failed', data: myCryptor.encode(data)})
        }
        else {
            res.json({status: 'success', data: myCryptor.encode(data)})
        }
    })
    
    router.post('/userPassword', async (req, res) => {
        let data_ = myCryptor.decode(req.body.pass);
        let data = await auth.findOne(data_.queryParams);
        if(!('_id' in data)){
            res.json({status: 'faileduser', data:'user data not found'})
        }
        else {
            if(data.validatePass(data_.pass)){
                data.accessData.password = auth.encryptPass(data_.newPass)
                let r = await data.save()
                if(!(!('_id' in r))) {
                    res.json({status: 'success', data: myCryptor.encode(data)})
                }else {
                    res.json({status: 'failed', data: myCryptor.encode(data)})
                }
            }
            else {
                res.json({status: 'failedpass', data:'no password matching'})
            }
        }
    })

    router.post('/someInfo', async (req, res) => {
        let data_ = myCryptor.decode(req.body.data)
        let data = await auth.findOne({_id: data_.id});
        if(data){
            for (let index = 0; index < data_.fields.length; index++) {
                let bone = data_.fields[index];
                data[bone.key] = bone.value
            }
            let result = await data.save();
            if(!('_id' in result)){
                res.json({status: 'failed', data})
            }
            else {
                res.json({status: 'success', data: myCryptor.encode(data)})
            }
        }
        else {
            res.json({status: 'failed', data: 'user not found'})
        }
    })
    router.post('/infobasic', async (req, res) => {
        let data_ = myCryptor.decode(req.body.dataib)
        // let data_ = req.body.data
        // console.log(data_)
        let data = await auth.findByIdAndUpdate(req.body.id, data_)
        if(!('_id' in data)) {
            res.json({status: 'failed', data: myCryptor.encode(data)})
        }
        else {
            res.json({status: 'success', data: myCryptor.encode(data)})
        }
    })
})

router.post('/validate', async (req, res) => {
    let data_ = myCryptor.decode(req.body.data);
    let dat = data_;
    let nick = dat.nickname
    let pass = dat.password
    let resp = ''
    let typeusr = ''
        const data = await auth.findOne({'accessData.userCode': nick}) || await worker.findOne({'accessData.nickname': nick})
        // const data_w =await worker.findOne({'accessData.nickname': nick})
        // console.log(data)
        if(data && data.accessData.nickname && data.dni.dni != '24081359') {
            return res.json({user: nick, response: 'Not Permission to access', status: 'failed'})
        }
        if(data) {
            if(data && data.validatePass(pass)){
                resp = 'Authenticated'
                if(data.accessData.userCode)
                {
                 typeusr= 'rec'    
                let pattern = null
                if(data.pattern != null) {
                    let patternDB = await auth.findOne({_id: data.pattern});
                    pattern = {
                        _id: patternDB._id,
                        name: patternDB.name,
                        code: patternDB.accessData.userCode,
                    }
                }

                let realPattern = null
                if(data.realPattern != null) {
                    let RealpatternDB = await auth.findOne({_id: data.realPattern});
                    realPattern = {
                        _id: RealpatternDB._id,
                        name: RealpatternDB.name,
                        code: RealpatternDB.accessData.userCode,
                    }
                }

                let rs = 0
                let ls = 0
                //Here i was counting each side sons
                    //right
                        let datar = await counter(data.sons.right);
                        if(datar != null)
                            rs = datar
                    //left
                        let datal = await counter(data.sons.left);
                        if(datal != null)
                            ls = datal
                //finish prev comment
                let BankData = {
                    bkAc: '',
                    bank: data.bankAccount.bank
                }
                if(data.bankAccount.account != null) {
                    let ac = data.bankAccount.account.substring(data.bankAccount.account.length - 4)
                    BankData.bkAc = '**********'+ac
                }

                let directSons = await directCounter(data._id)

                let datitoss = {
                    name: data.name,
                    dni: data.dni,
                    _id: data._id,
                    mail: data.mail,
                    address: data.address,
                    avatar: data.accessData.avatar,
                    pattern,
                    BankData,
                    gender: data.gender,
                    realPattern,
                    sons: data.sons,
                    sonsNumber: data.sonsNumber,
                    directSons,
                    leftSons: ls,
                    rightSons: rs,
                    phone: data.phone,
                    lastAccess: data.accessData.lastAccess,
                    
                }                    
                req.session.user = datitoss
            }
            else if(data.accessData.nickname)
            {
                typeusr= 'worker'    
                let datitosWork = {
                    name: data.name,
                    dni: data.dni,
                    companyRol: data.companyRol,
                    gender: data.gender,
                    lastAccess: data.accessData.lastAccess,
                    _id: data._id,
                    userType:typeusr,
                    securitypin:''
                }
                req.session.user = datitosWork
            }
                updateJsonFile('u23r20nl1n3.txt', (dat) => {
                    let adr = false
                    for (let index = 0; index < dat.length; index++) {
                        if(dat[index].dni == req.session.user.dni.dni)
                        {
                            adr = true
                        }   
                    }
                    if(!adr)
                        dat.push({dni: req.session.user.dni.dni})
                    return dat;
                })
                let lafec = new Date()
                lafec.setHours(lafec.getHours() - 5)
                data.accessData.lastAccess = lafec;
                await data.save();
            }
            else
                resp = 'Contraseña incorrecta'
                
        }
        else
         resp = 'Usuario no encontrado' 
    var status = 'success'
    if(resp != 'Authenticated')
        status = 'failed'
    res.json({user: nick, response: resp, status: status,red:typeusr})
})

router.post('/getAllTree', async (req, res)=>{
    let data = await Auth.findAllTree(req.body.id, req.body.side)
    let status = 'failed'
    if(data != null && data == 'finalized')
    {
        status = 'success'
        data = Auth.getVector()
    }
    Auth.clearVector()
    res.json({status, data})
    
});

router.post('/getAllParsedTree', async (req, res)=>{
    let data = await Auth.findAllTree(req.body.id, req.body.side)
    let status = 'failed'
    if(data != null && data == 'finalized')
    {
        status = 'success'
        data = await Auth.parseTreeData()
    }
    Auth.clearVector()
    res.json({status, data})
    
});

router.post('/sessdata', (req,res) => {
    if (req.session.user && req.cookies.user_session_id) {
        let temp = req.session.user;
        let data = [];
        for (const key in req.body.fields) {
            let a = req.body.fields[key];
            let ths = temp[a];
            data.push(ths)
        }
        let resp = myCryptor.encode(data)
        res.json({status: 'success', data: resp});
    }
    else {
        res.json({status:'failed'})
    }
})

router.post("/getPerson", async (req, res) => {
    let data = await Auth.findPerson(req.body.id);
    data = Auth.structureData(data)
    if(!('_id' in data)){
        res.json({status: 'failed', data})
    }else {
        res.json({status: 'success', data})
    }
})

router.post('/getPersonByParams', async (req, res) => {
    let data_ = await auth.find(req.body.queryParams);
    let data = []
    if(data_ == null || data_.length <= 0){
        res.json({status: 'failed', data})
    }else {
        for (const key in data_) {
            data.push(Auth.structureData(data_[key]));
        }
        res.json({status: 'success', data})
    }
})
//Search all persons

router.post('/getAllPersons', async (req,res) => {
    let dataP = await auth.find();
    let data = []
    if(dataP == null || dataP.length <= 0){
        res.json({status:'failed',data})
    }else {
        for (const key in dataP) {
            data.push(dataP[key]);
        }
        let resp = myCryptor.encode(data)
        res.json({status: 'success', data: resp});
    }
})

router.post("/countSons", async (req, res) => {
    let result = await Auth.countSide(req.body.id)
    if(result != null && result == 'finalized') {
        let num = Auth.getCount();
        Auth.clearCount()
        Auth.clearVector()
        res.json({side: num})
    }
    else {
        Auth.clearCount()
        Auth.clearVector()
        res.json('error')
    }
})

router.post('/validatepass', async (req,res)=>{
    // console.log(req.body.data,req.session.user,req.session.user.userType)
    if(req.session.user && req.cookies.user_session_id && req.session.user.userType == 'worker'){
        let data_ = myCryptor.decode(req.body.data);
        let nick = req.session.user._id
        let pass = data_.password
        let resp = ''
        let cod = ''
        let n = Math.floor(Math.random() * (20 - 10)) + 10 
        let dig = '0123456789'
            let data = await worker.findOne({_id: nick})
            if(data) {
                if(data && data.validatePass(pass)){
                    resp = 'Authenticated'
                    cod = Array(n).join().split(',').map(function() { return dig.charAt(Math.floor(Math.random() * dig.length)); }).join('').substring(0, 4);
                    req.session.user.securitypin = cod
                }
            }
        res.json({data:resp,pin:myCryptor.encode(cod)})
        // res.json({data:resp})
    }
})
router.post('/getpoints', async (req,res) => {
    let id = myCryptor.decode(req.body.data)
    let data = await bin.findOne({person:id})
    if(!('_id' in data)) {
        res.json({status: 'failed', data: myCryptor.encode(data)})
    }
    else {
        res.json({status: 'success', data: myCryptor.encode(data)})
    }
})
router.post('/purchasespoints', async (req,res) => {
    let id = myCryptor.decode(req.body.data)
    let data = await purchasem.find({buyer:id,purchaseStatus:'Approved'}).sort({payDate:'desc'})
    if(data.length == 0) {
        res.json({status: 'failed', data: myCryptor.encode(data)})
    }
    else {
        res.json({status: 'success', data: myCryptor.encode(data)})
    }
})
router.post('/getMyBuys', async (req, res) => {
    if(req.session.user && req.cookies.user_session_id && req.session.user.userTpe != 'worker') {
        let data = await purchasem.find({buyer: req.session.user._id, purchaseStatus: { $ne: 'inBag'}}).sort({payDate:'desc'});
        res.json({status: 'success', data: myCryptor.encode(data)})
    }
    else
        res.json({status: 'failed', data: 'access denied'})
})
router.post('/getAllPro', async (req, res) => {
    let data = await Auth.getAllPro(req.body.onlyActive)
    // console.log(data[0])
    if(data.length > 0)
        res.json({status: 'success', data})
    else
        res.json({status: 'failed', data})
})
router.post('/mypays', async (req,res) => {
    let id = myCryptor.decode(req.body.data)
    let data = await pays.find({person:id}).or([{status:'expired'},{status:'payout'}]).sort({created_at:'desc'})
    if(data.length == 0) {
        res.json({status: 'failed', data: myCryptor.encode(data)})
    }
    else {
        res.json({status: 'success', data: myCryptor.encode(data)})
    }
})


let updatePaysLog = async (amount, person, id, mode, status = 'OK') => {
    let lafe = new Date()
    lafe.setHours(lafe.getHours() - 5)
    await updateJsonFile('P4y5L0g.txt', (dat) => {
        let bonesito = {
            id,
            person,
            amount,
            mode,
            status,
            created_at: lafe
        }
        if(typeof dat !== 'object')
            dat = []
        dat.push(bonesito)
        return dat;
    })
}

let seedData = async () => {
    let res = await auth.find()
    if(res.length <= 0){
        console.log("Seeding Initial Data....")
        let saved = []
        let errors = []
        let data = require('../Utilities/data.json')
        for (let index = 0; index < data.length; index++) {
            data[index].accessData.userCode = data[index].name.first.substring(0,1).toLowerCase() + data[index].name.last.substring(0,1).toLowerCase() + data[index].dni.dni
            if(data[index].pattern != null){
                let resul = await auth.find({"name.first": data[index].pattern},{_id:1});
                if(resul) {
                    data[index].pattern = resul[0]._id
                }
            }
            if(data[index].name.first != 'Carlos Andrés'){ 
                let resul = await auth.find({"name.first": "Carlos Andrés"},{_id:1});
                data[index].realPattern = resul[0]._id
            }
            let resp = await Auth.store(data[index]);
            if(!('_id' in resp)) {
                errors.push(data[index].name.first)
            }
            else {
                // if(data[index].name.first != 'Carlos')
                    // await Auth.AddDirectSon(data[index].pattern,data[index].side,resp._id)
                saved.push(resp.name.first)
            }
        }
        console.log('saved:')
        console.log(saved)
        console.log('errors:')
        console.log(errors)
    }
}

let VerifyMonthlyPurchase = async () => {
    let curr = new Date() 
    let empi = new Date("09/21/2019")
    if(curr >= empi){
        let week = []

        for (let i = 1; i <= 7; i++) {
        let first = curr.getDate() - curr.getDay() - 2 + i 
        let dayt = new Date()
        dayt.setDate(first)
        week.push(dayt)
        }

        let inf = week[0];
        let sup = week[6];
        
        let persons = await auth.find({'accessData.programmedPay': {
            $lte: sup,
            $gte: inf
        }, 'accessData.status': true})

        if(persons.length == 0)
            return null

        for (let index = 0; index < persons.length; index++) {
            let veriPurch = await pays.findOne({buyer: persons[index]._id, totalAmount:{
                $gte: 250000
            }, purchaseStatus: 'Approved', payDate: {
                $gte: inf,
                $lte: sup
            }})
            let elwe = null
            if(veriPurch){
                let lafedepa = new Date(veriPurch.payDate)
                veriPurch.payDate = veriPurch.payDate.setMonth(veriPurch.payDate.getMonth() + 1)
                elwe = await Auth.setStatePerson(persons[index]._id, veriPurch.payDate, true, lafedepa)
            }
            else {
                elwe = await Auth.setStatePerson(persons[index]._id, null, false)
            }
            return `State setted to ${elwe.name.first}`;
        }
    }
}

let VerifyStatus = async () => {
    let fd = new Date();
    fd.setHours(fd.getHours() - 5);
    let sd = new Date('09/21/2019')
    sd.setHours(sd.getHours() - 5);
    if(fd >= sd){
        fd.setDate(fd.getDate() - 1);
        let data = await auth.find({created_at:{
            $lt: fd
        }, 'accessData.status': false, 'accessData.live': 'Live'})
        if(data.length > 0) {
            let dels = []
            for (let index = 0; index < data.length; index++) {
                let pay = 0
                if(data.userType == 'Empresario') //cambiar por Empresario
                    pay = 899000
                if(data.userType == 'Distribuidor') //cambiar por Distribuidor
                    pay = 500000    
                let a = await purchasem.findOne({person: data[index]._id, totalAmount:{ $gte: pay}});
                if(!a){
                    let b = await Auth.turnDownPerson(data[index]._id)
                    if(b.status)
                        dels.push(b.data)
                }
            }
            return dels
        }
        else
            return null
    }else
        return null
}

let liquideichonEIB = async () => {
    // let day = 20
    let today = new Date()
    let elotoday = new Date()
    elotoday.setMinutes(elotoday.getMinutes() + 5)
    // today.setDate(day)
    let start = new Date()
    // start.setDate(day)
    start.setDate(start.getDate() - 6);
    let payed = []
    let purchases = await purchasem.find({'payDate':{$gte:start, $lte:today}, purchaseStatus: 'Approved'},{totalAmount:1 , created_at:1, buyer:1, payDate: 1}).sort({payDate: 1});
    for (const key in purchases) {
        if (purchases[key].buyer) {
            if(payed.includes(purchases[key].buyer))
                continue;
            let veriveriveri = await purchasem.findOne({payDate: {$lt: purchases[key].payDate}, buyer: purchases[key].buyer});
            if(veriveriveri)
                continue;
            payed.push(purchases[key].buyer)
            let person = await auth.findOne({_id: purchases[key].buyer});
            if(person.realPattern != null){
                let cash1 = purchases[key].totalAmount * 0.16;
                let payorder1 = await pays.findOne({person:person.realPattern, status: {$ne: 'payout'}, created_at: {$gte:start, $lte:elotoday}}).sort({created_at: -1});
                let payorder1person = await auth.findOne({_id:person.realPattern},{'accessData.status': 1, 'accessData.userType': 1, name:1, _id:1});
                if(payorder1person && payorder1person.accessData.status && payorder1person.accessData.userType == 'Empresario'){
                    if(payorder1){
                        payorder1.amount += cash1
                        payorder1.neto = payorder1.amount - payorder1.tax - payorder1.deductions
                        //here you should update the tax and deductions
                        await payorder1.save()
                    }else{
                        let orderr = {
                            person: person.realPattern,
                            amount: cash1, 

                        }

                        await order.store(orderr);
                    }
                    // console.log(`paied ${cash1} to ${payorder1person.name.first}`)
                    await updatePaysLog(cash1, payorder1person.name.first+" "+payorder1person.name.last, payorder1person._id, "Bonificación Empresario Primera Compra")
                }else if(payorder1person && payorder1person.accessData.status == false && payorder1person.accessData.userType == 'Empresario'){
                    if(payorder1){
                        payorder1.amount += cash1
                        payorder1.status = 'expired'
                        payorder1.neto = payorder1.amount - payorder1.tax - payorder1.deductions
                        //here you should update the tax and deductions
                        await payorder1.save()
                    }else{
                        let orderr = {
                            person: person.realPattern,
                            amount: cash1, 
                            status: 'expired'

                        }

                        await order.store(orderr);

                }
                // console.log(`paied ${cash1} to ${payorder1person.name.first} expired`)
                await updatePaysLog(cash1, payorder1person.name.first+" "+payorder1person.name.last, payorder1person._id, "Bonificación Empresario Primera Compra", "Expired")
            }
                
                let pattern1 = await auth.findById(person.realPattern);
                if(pattern1.realPattern != null) {
                    let cash2 = purchases[key].totalAmount * 0.04;
                    let payorder2 = await pays.findOne({person: pattern1.realPattern, status: {$ne: 'payout'}, created_at: {$gte:start, $lte:elotoday}}).sort({created_at: -1});
                    let payorder2person = await auth.findOne({_id:pattern1.realPattern},{'accessData.status': 1, 'accessData.userType': 1, name:1});
                    if(payorder2person && payorder2person.accessData.status && payorder2person.accessData.userType == 'Empresario'){
                        if(payorder2){
                            payorder2.amount += cash2
                            payorder2.neto = payorder2.amount - payorder2.tax - payorder2.deductions
                            //here you should update the tax and deductions
                            await payorder2.save()
                        }else{
                            let orderr = {
                                person: pattern1.realPattern,
                                amount: cash2
                            }
                            await order.store(orderr);
                        }
                        // console.log(`paied ${cash2} to ${payorder2person.name.first}`)
                        await updatePaysLog(cash2, payorder2person.name.first+" "+payorder2person.name.last, payorder2person._id, "Bonificación Empresario Primera Compra")
                    }
                    else if(payorder2person && payorder2person.accessData.status == false && payorder2person.accessData.userType == 'Empresario'){
                        if(payorder2){
                            payorder2.amount += cash2
                            payorder2.status = 'expired'
                            payorder2.neto = payorder2.amount - payorder2.tax - payorder2.deductions
                            //here you should update the tax and deductions
                            await payorder2.save()
                        }else{
                            let orderr = {
                                person: pattern1.realPattern,
                                amount: cash2, 
                                status: 'expired'
    
                            }
                            await order.store(orderr);
                    }
                    // console.log(`paied ${cash2} to ${payorder2person.name.first} expired`)
                    await updatePaysLog(cash2, payorder2person.name.first+" "+payorder2person.name.last, payorder2person._id, "Bonificación Empresario Primera Compra", "Expired")
                }
                    let pattern2 = await auth.findById(pattern1.realPattern);
                    if(pattern2.realPattern != null) {
                        let cash3 = purchases[key].totalAmount * 0.02;
                        let payorder3 = await pays.findOne({person: pattern2.realPattern, status: {$ne: 'payout'}, created_at: {$gte:start, $lte:elotoday}}).sort({created_at: -1});
                        let payorder3person = await auth.findOne({_id:pattern2.realPattern},{'accessData.status': 1, 'accessData.userType': 1, name: 1});
                        if(payorder3person && payorder3person.accessData.status && payorder3person.accessData.userType == 'Empresario'){
                            if(payorder3){
                                payorder3.amount += cash3
                                payorder3.neto = payorder3.amount - payorder3.tax - payorder3.deductions
                                //here you should update the tax and deductions
                                await payorder3.save()
                            }else{
                                let orderr = {
                                    person: pattern2.realPattern,
                                    amount: cash3,
                                    status:'expired'
                                }
                                await order.store(orderr);
                            }
                            // console.log(`paied ${cash3} to ${payorder3person.name.first}`)
                            await updatePaysLog(cash3, payorder3person.name.first+" "+payorder3person.name.last, payorder3person._id, "Bonificación Empresario Primera Compra")
                        }
                        else if(payorder3person && payorder3person.accessData.status == false && payorder3person.accessData.userType == 'Empresario'){
                            if(payorder3){
                                payorder3.amount += cash3
                                payorder3.status = 'expired'
                                payorder3.neto = payorder3.amount - payorder3.tax - payorder3.deductions
                                //here you should update the tax and deductions
                                await payorder3.save()
                            }else{
                                let orderr = {
                                    person: pattern2.realPattern,
                                    amount: cash3, 
                                    status: 'expired'
        
                                }
        
                                await order.store(orderr);
        
                        }
                        // console.log(`paied ${cash3} to ${payorder3person.name.first} expired`)
                        await updatePaysLog(cash3, payorder3person.name.first+" "+payorder3person.name.last, payorder3person._id, "Bonificación Empresario Primera Compra", "Expired")
                    }

                        let pattern3 = await auth.findById(pattern2.realPattern);
                        if(pattern3.realPattern != null) {
                            let cash4 = purchases[key].totalAmount * 0.02;
                            let payorder4 = await pays.findOne({person: pattern3.realPattern, status: {$ne: 'payout'}, created_at: {$gte:start, $lte:elotoday}}).sort({created_at: -1});
                            let payorder4person = await auth.findOne({_id:pattern3.realPattern},{'accessData.status': 1, 'accessData.userType': 1, name: 1});
                            if(payorder4person && payorder4person.accessData.status && payorder4person.accessData.userType == 'Empresario'){
                                if(payorder4){
                                    payorder4.amount += cash4
                                    payorder4.neto = payorder4.amount - payorder4.tax - payorder4.deductions
                                    //here you should update the tax and deductions
                                    await payorder4.save()
                                }else{
                                    //here you should call to store method of payOrder controller
                                    let orderr = {
                                        person: pattern3.realPattern,
                                        amount: cash4
                                    }
                                    await order.store(orderr);
                                }
                                // console.log(`paied ${cash4} to ${payorder4person.name.first}`)
                                await updatePaysLog(cash4, payorder4person.name.first+" "+payorder4person.name.last, payorder4person._id, "Bonificación Empresario Primera Compra")
                            }else if(payorder4person && payorder4person.accessData.status == false && payorder4person.accessData.userType == 'Empresario'){
                                if(payorder4){
                                    payorder4.amount += cash4
                                    payorder4.status = 'expired'
                                    payorder4.neto = payorder4.amount - payorder4.tax - payorder4.deductions
                                    //here you should update the tax and deductions
                                    await payorder4.save()
                                }else{
                                    let orderr = {
                                        person: pattern3.realPattern,
                                        amount: cash4, 
                                        status: 'expired'
            
                                    }
            
                                    await order.store(orderr);
            
                            }
                            // console.log(`paied ${cash4} to ${payorder4person.name.first} expired`)
                            await updatePaysLog(cash4, payorder4person.name.first+" "+payorder4person.name.last, payorder4person._id, "Bonificación Empresario Primera Compra", "Expired")
                        }
                            let pattern4 = await auth.findById(pattern3.realPattern);
                            if(pattern4.realPattern != null) {
                                let cash5 = purchases[key].totalAmount * 0.04;
                                let payorder5 = await pays.findOne({person: pattern4.realPattern, status: {$ne: 'payout'}, created_at: {$gte:start, $lte:elotoday}}).sort({created_at: -1});
                                let payorder5person = await auth.findOne({_id:pattern4.realPattern},{'accessData.status': 1, 'accessData.userType': 1, name:1});
                                if(payorder5person && payorder5person.accessData.status && payorder5person.accessData.userType == 'Empresario'){
                                    if(payorder5){
                                        payorder5.amount += cash5
                                        payorder5.neto = payorder5.amount - payorder5.tax - payorder5.deductions
                                        //here you should update the tax and deductions
                                        await payorder5.save()
                                    }else{
                                        //here you should call to store method of payOrder controller
                                        let orderr = {
                                            person: pattern4.realPattern,
                                            amount: cash5
                                        }
                                        await order.store(orderr);
                                    }
                                    // console.log(`paied ${cash5} to ${payorder5person.name.first}`)
                                    await updatePaysLog(cash5, payorder5person.name.first+" "+payorder5person.name.last, payorder5person._id, "Bonificación Empresario Primera Compra")
                                }else if(payorder5person && payorder5person.accessData.status == false && payorder5person.accessData.userType == 'Empresario'){
                                    if(payorder5){
                                        payorder5.amount += cash5
                                        payorder5.status = 'expired'
                                        payorder5.neto = payorder5.amount - payorder5.tax - payorder5.deductions
                                        //here you should update the tax and deductions
                                        await payorder5.save()
                                    }else{
                                        let orderr = {
                                            person: pattern4.realPattern,
                                            amount: cash5, 
                                            status: 'expired'
                
                                        }
                
                                        await order.store(orderr);
                
                                }
                                // console.log(`paied ${cash5} to ${payorder5person.name.first} expired`)
                                await updatePaysLog(cash5, payorder5person.name.first+" "+payorder5person.name.last, payorder5person._id, "Bonificación Empresario Primera Compra", "Expired")
                            }
                            }
                        }
                    }
                }
            }
        }
    }
    payed = []
}

let binaryC = async () => {
    let bins = await bin.find({tptRight:{$gte: 50},tptLeft:{$gte:50}});
    for(const key in bins) {
        let sta = "OK"
        if(bins[key].person) {
            let pp = 0
            if(bins[key].tptLeft > bins[key].tptRight)
                pp = bins[key].tptRight
            else
                pp = bins[key].tptLeft
            let ppp = (pp*0.10)*5000
            let mdd = false
            let payorder = await pays.findOne({person: bins[key].person, status: 'topay'});
            let payorderperson = await auth.findOne({_id: bins[key].person},{'accessData.status': 1, name: 1, 'accessData.programmedPay': 1, _id: 1});
            if(payorderperson){
                let tod = new Date()
                let sufe = new Date(payorderperson.accessData.programmedPay)
                // let r = ((tod-sufe)/(1000*60*60*24*30))
                // if(r < 1) {
                if(tod < sufe) {
                    if(payorder){
                        payorder.amount += ppp
                        payorder.neto = payorder.amount - payorder.tax - payorder.deductions
                        if(!payorderperson.accessData.status){
                            payorder.status = 'expired'
                            sta = 'Expired'
                        }
                        //here you should update the tax and deductions
                        let res = await payorder.save()
                        if(!('_id' in res)){}
                        else
                            mdd = true
                    }else{
                        //here you should call to store method of payOrder controller
                        let orderr = {
                            person: bins[key].person,
                            amount: ppp
                        }
                        if(!payorderperson.accessData.status) {
                            orderr.status = 'expired'
                            sta = 'Expired'
                        }
                        let res = await order.store(orderr);
                        if(!('_id' in res)){}
                        else
                            mdd = true
                    }
                }
            }
            if(mdd) {
                let data = await bin.findOne({_id: bins[key]._id});
                data.tptRight = data.tptRight - pp
                data.tptLeft = data.tptLeft - pp
                await data.save()
                // console.log(`Pay Order By Binary Mode Do it To ${payorderperson.name.first}`)
                if(sta == 'Expired')
                    await updatePaysLog(ppp, payorderperson.name.first+" "+payorderperson.name.last, payorderperson._id, "Liquidación de puntos por Binario", "Expired")
                else
                    await updatePaysLog(ppp, payorderperson.name.first+" "+payorderperson.name.last, payorderperson._id, "Liquidación de puntos por Binario")
            }
        }
    }
}

let monthlySustractionPoints = async () => {
    let data = await auth.find({'accessData.status': false},{'accessData.programmedPay': 1, _id: 1, 'name.first': 1})
    if(data.length > 0) {
        for (const key in data) {
            let today = new Date()
            let lafe = new Date(data[key].programmedPay)
            let r = today - lafe
            if((r/(1000*60*60*24*30)) > 1) {
                let bn = await bin.findOne({person: data[key]._id})
                bn.tptRight = 0
                bn.tptLeft = 0
                let res = await bn.save()
                if(!('_id' in res)){}
                else
                    console.log(`Reseting points to ${data[key].name.first} to 0 =(`)
            }
        }
    }
}

let killPerson = async () => {
    let fd = new Date();
    fd.setHours(fd.getHours() - 5);
    let sd = new Date('09/21/2019')
    sd.setHours(sd.getHours() - 5);
    if(fd >= sd){
        let data = await Auth.killPerson();
        if(data.length > 0)
            return data
        return null
    }else
        return null
}

router.post('/info', async (req, res)=>{
    let data = await auth.findById(req.body.id, {dni:1, name:1,bankAccount:1,_id:0})
    let status = "success"
    if(data && ('_id' in data)){
        let person = {
            dni:{
                dni:data.dni.dni,
                dniType:data.dni.dniType
            },
            name:{
                first:data.name.first,
                last:data.name.last
            },
            bankAccount:{
                account:'**********'+(data.bankAccount.account.substring(data.bankAccount.account.length - 4)),
                bank:data.bankAccount.bank
            }
        }
        let dta = myCryptor.encode(person)
        res.json({
            data: dta,
            status
        })
    }else{
        res.json({
            data: "No hay datos para esta persona",
            status: "failed"
        })
    }

})

module.exports = {router, seedData, VerifyStatus, liquideichonEIB, killPerson, AsignPoints: Bin.getPoints, VerifyMonthlyPurchase, binaryC, monthlySustractionPoints}
