const { Schema, model } = require('mongoose');

let ColombianDate = new Date()
ColombianDate.setHours(ColombianDate.getHours() - 5)
const Product = new Schema({
    name: {
        type: String,
        required: true
    },
    code:String,
    sanitaryReg:String,
    description: {
        type: String,
        required: true
    },
    slogan: {
        type: String,
        default: null
    },
    prices: {
        public: {
            type: Number,
            required: true
        },
        rec: {
            type: Number,
            required: true
        }
    },
    stock: {
        type: Number,
        default: 0
    },
    status: {
        type: Boolean,
        default: true
    },
    images: [String],
    category: {
        type: Schema.Types.ObjectId,
        default: null
    },
    created_at: {
        type: Date,
        default: ColombianDate
    }
});

module.exports = model('product', Product)