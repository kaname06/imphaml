const { Schema, model } = require('mongoose');

const Category = new Schema({
    name: {
        type: String,
        required: true
    },
    subName: {
        type: String,
        default: null
    },
    description: {
        type: String,
        default: null
    },
    image: {
        type: String,
        // required: true
    }
})

module.exports = model('category', Category)