const {Schema, model} = require('mongoose');

const Purchase = new Schema({
    buyer: {
        type: Schema.Types.ObjectId,
        required: true
    },
    orderDetails: [{
        product: {
            type: Schema.Types.ObjectId,
            required: true
        },
        quantity: {
            type: Number,
            required: true
        }
    }],
    paymentRef: {
        type: String,
        default: null
    },
    totalAmount: {
        type: Number,
        required: true
    },
    totalPoints: {
        type: Number,
        default: 0
    },
    pointsUsed:{
        type: Boolean,
        default: false
    },
    purchaseType: {
        type: String,
        default: 'public',
        enum: ['public', 'rec']
    },
    purchaseStatus: {
        type: String,
        default: 'inBag',
        enum: ['inBag', 'Pending', 'Approved', 'Rejected']
    },
    internalStatus: {
        status: {
            type: String,
            default: 'waiting',
            enum: ['waiting', 'ready', 'delivered', 'received']
        },
        deliveryDate: {
            type: Date,
            default: null
        },
        receivedDate: {
            type: Date,
            default: null
        },
        receiverName: {
            type: String,
            default: null
        }
    },
    created_at: {
        type: Date,
        default: Date.now
    }
},{ collection : 'purchases' });

module.exports = model('purchase', Purchase);