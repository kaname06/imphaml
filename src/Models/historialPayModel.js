const {Schema, model} = require('mongoose')

const Pay = new Schema({
    person: {
        type: Schema.Types.ObjectId,
        required: true
    },
    mount: {
        type: String,
        required: true
    },
}, {timestamps: true})

module.exports = model('Pay', Pay);