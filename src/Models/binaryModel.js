const {Schema, model} = require('mongoose')

const Binary = new Schema({
    person: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'auth'
    },
    ownP:{
        type: Number,
        required: true
    },
    downP:{
        type: Number,
        default: 0
    },
    tptRight:{
        type: Number,
        default: 0
    },
    tptLeft:{
        type: Number,
        default: 0
    },
    ppp:{
        type: Number,
        default: 0
    },
    hpRight:{
        type: Number,
        default: 0
    },
    Left:{
        type: Number,
        default: 0
    }
})

module.exports = model('Binary',Binary);