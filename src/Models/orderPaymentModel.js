const {Schema, model} = require('mongoose')

const Order = new Schema({
    person: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'auth'
    },
    amount: {
        type: Number,
        required: true
    },
    tax:{
        type: Number,
        default: 0
    },
    deductions:{
        type: Number,
        default: 0
    },
    deductionsDetails:[{
        concept: String,
        percent:Number
    }],
    neto:{
        type: Number,
        default: function(){
            return (this.amount-this.tax-this.deductions)
        }
    },
    status:{
        type:String,
        default:'topay',
        enum:['topay','payout','expired']
    },
    created_at: {
        type: Date,
        default: Date.now
    }
})

module.exports = model('Order',Order);