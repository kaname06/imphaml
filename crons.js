const cron = require('node-cron')
const { Bin } = require('./src/Controllers/binaryController')
const bin = require('./src/Models/binaryModel')

//task to clean DB about time out pay persons
cron.schedule('55 4 * * *', async () => {
    let a = new Date()
    // a.setHours(a.getHours() - 5)
    console.log(`(${a.getHours()}:${a.getMinutes()} at ${a.getDate()}/${a.getMonth() + 1}/${a.getFullYear()}) Running "Clean time out pay Persons" Scheduled task...`)
    let r = await require('./src/Routes/auth').VerifyStatus()
    if(r)
        console.log(r)
});

//task to clean DB about inactive persons
cron.schedule('58 4 31 12 *', async () => {
    let a = new Date()
    // a.setHours(a.getHours() - 5)
    console.log(`(${a.getHours()}:${a.getMinutes()} at ${a.getDate()}/${a.getMonth() + 1}/${a.getFullYear()}) Running "Clean inactive Persons" Scheduled task...`)
    let r = await require('./src/Routes/auth').killPerson()
    if(r)
        console.log(r)
});

//task to asign points
cron.schedule('57 4 * * *', async () => {
    let a = new Date()
    // a.setHours(a.getHours() - 5)
    console.log(`(${a.getHours()}:${a.getMinutes()} at ${a.getDate()}/${a.getMonth() + 1}/${a.getFullYear()}) Running "Asign points to persons" Scheduled task...`)
    // let r = await require('./src/Routes/auth').AsignPoints()
    let r = await Bin.getPoints()

    console.log(`(${a.getHours()}:${a.getMinutes()} at ${a.getDate()}/${a.getMonth() + 1}/${a.getFullYear()}) Running "Monthly Sustract points to inactive persons" Scheduled task...`)
    let r1 = await require('./src/Routes/auth').monthlySustractionPoints()
})
//task to verify Monthly Payments
cron.schedule('50 4 * * 5', async () => {
    let a = new Date()
    // a.setHours(a.getHours() - 5)
    console.log(`(${a.getHours()}:${a.getMinutes()} at ${a.getDate()}/${a.getMonth() + 1}/${a.getFullYear()}) Running "Monthly paymment Verification" Scheduled task...`)
    let r = await require('./src/Routes/auth').VerifyMonthlyPurchase()
    if(r)
        console.log(r)
});

//task to Liquidate Cash to do Pay Orders
cron.schedule('59 4 * * *', async ()=>{
    let a = new Date()
    // a.setHours(a.getHours() - 5)
    console.log(`(${a.getHours()}:${a.getMinutes()} at ${a.getDate()}/${a.getMonth() + 1}/${a.getFullYear()}) Running "Each friday liquidation method (Binary - Enterprise)" Scheduled task...`)
    await require('./src/Routes/auth').liquideichonEIB()
    await require('./src/Routes/auth').binaryC()
})